root = exports ? this

validateForms = ->
    Validators
        "#lemail": new EmailValidator("Please enter a valid email address")
        "#lpass": new LengthValidator(6, "The password should be atleast 6 characters")

validateForms2 = ->
    Validators
        "#remail,#rname,#rpass": new EmptyValidator()
        "#remail": new EmailValidator("Please enter a valid email address")
        "#rpass": new LengthValidator(6, "The password should be atleast 6 characters")



hideErrors = -> $('.error-block').hide()
    
$('lform').submit validateForms
$('rform').submit validateForms