validateProblem = ->
    Validators
        "input": new EmptyValidator()
        "#weight": new RegexValidator 'error', 'Please enter an integer', regex = /\d+/


validateTestcase = ->
    Validators
        "input": new EmptyValidator()
        "#problem_id": new RegexValidator 'error', 'Please enter a valid integer', regex=/\d+/


$('form.problem').submit validateProblem
$('form.testcase').submit validateTestcase