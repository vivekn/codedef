(function() {
  var hideErrors, root, validateForms, validateForms2;

  root = typeof exports !== "undefined" && exports !== null ? exports : this;

  validateForms = function() {
    return Validators({
      "#lemail": new EmailValidator("Please enter a valid email address"),
      "#lpass": new LengthValidator(6, "The password should be atleast 6 characters")
    });
  };

  validateForms2 = function() {
    return Validators({
      "#remail,#rname,#rpass": new EmptyValidator(),
      "#remail": new EmailValidator("Please enter a valid email address"),
      "#rpass": new LengthValidator(6, "The password should be atleast 6 characters")
    });
  };

  hideErrors = function() {
    return $('.error-block').hide();
  };

  $('lform').submit(validateForms);

  $('rform').submit(validateForms);

}).call(this);
