(function() {
  var validateProblem, validateTestcase;

  validateProblem = function() {
    return Validators({
      "input": new EmptyValidator(),
      "#weight": new RegexValidator('error', 'Please enter a floating point no or integer', /[-+]?[0-9]*\.?[0-9]+([eE][-+]?[0-9]+)?/)
    });
  };

  validateTestcase = function() {
    return Validators({
      "input": new EmptyValidator(),
      "#problem_id": new RegexValidator('error', 'Please enter a valid integer', /[-+]?[0-9]*\.?[0-9]+([eE][-+]?[0-9]+)?/)
    });
  };

  $('form').submit(newProblem);

}).call(this);
