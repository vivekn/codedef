(function() {
  var validateProblem, validateTestcase;

  validateProblem = function() {
    var regex;
    return Validators({
      "input": new EmptyValidator(),
      "#weight": new RegexValidator('error', 'Please enter an integer', regex = /\d+/)
    });
  };

  validateTestcase = function() {
    var regex;
    return Validators({
      "input": new EmptyValidator(),
      "#problem_id": new RegexValidator('error', 'Please enter a valid integer', regex = /\d+/)
    });
  };

  $('form.problem').submit(validateProblem);

  $('form.testcase').submit(validateTestcase);

}).call(this);
