root = exports ? this

validateForms = ->
    Validators
        "#team,#answer": new EmptyValidator()

hideErrors = -> $('.error-block').hide()
    
$('form').submit validateForms