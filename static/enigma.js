(function() {
  var hideErrors, root, validateForms;

  root = typeof exports !== "undefined" && exports !== null ? exports : this;

  validateForms = function() {
    return Validators({
      "#team,#answer": new EmptyValidator()
    });
  };

  hideErrors = function() {
    return $('.error-block').hide();
  };

  $('form').submit(validateForms);

}).call(this);
