(function() {
  var root;

  root = typeof exports !== "undefined" && exports !== null ? exports : this;

  root.close_flash = function() {
    return $('.flash').hide();
  };

}).call(this);
