from functools import wraps
from flask import session, request, redirect, url_for, abort

def login_required(f):
    @wraps(f)
    def decorated_function(*args, **kwargs):
        if 'user' not in session:
            return redirect(url_for('authenticate', next=request.url))
        return f(*args, **kwargs)
    return decorated_function

def auth_required(f):
    @wraps(f)
    def decorated_function(*args, **kwargs):
        if 'user' not in session:
            abort(404)
        return f(*args, **kwargs)
    return decorated_function

def to_unicode(obj, encoding='utf-8'):
     if isinstance(obj, basestring):
         if not isinstance(obj, unicode):
             obj = unicode(obj, encoding)
     return obj

def unimap(container):
    """
    Converts all strings in the container to unicode values.
    """
    if isinstance(container, dict):
        for key in container:
            container[key] = to_unicode(container[key])
    else:
        # If container is an iterable
        return map(to_unicode, container)
