import os

try:
    from bundle_config import config as cfg
    SQLALCHEMY_DATABASE_URI = 'postgresql://%s:%s@%s:%s/%s' % (
    cfg['postgres']['username'],
    cfg['postgres']['password'],
    cfg['postgres']['host'],
    int(cfg['postgres']['port']),
    cfg['postgres']['database']
    )
except:
    if 'DATABASE_URL' in os.environ:
        SQLALCHEMY_DATABASE_URI = os.environ['DATABASE_URL']
    else:
        SQLALCHEMY_DATABASE_URI = 'sqlite:////home/vivek/projects/codedef/def.db'

ADMINS = ['mail@vivekn.co.cc', 'mail2@vivekn.co.cc', 'amit.solapurkar.ece09@itbhu.ac.in', 'solapurkar.amit@gmail.com']
TIMEFORMAT = "%m/%d/%Y %H:%M" # Time format returned by jquery date time picker
