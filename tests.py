import codedef
import unittest
from datetime import datetime, timedelta
from StringIO import StringIO

def date_to_str(dt):
	return dt.strftime(codedef.app.config['TIMEFORMAT'])

class Tests(unittest.TestCase):
	def setUp(self):
		codedef.app.config['TESTING'] = True
		self.app = codedef.app.test_client()
		codedef.db.create_all()
		self.user = codedef.User('mail2@vivekn.co.cc', 'foobar', 'Vivek Narayanan')
		codedef.db.session.add(self.user)
		codedef.db.session.commit()

	def tearDown(self):
		self.logout()
		codedef.db.drop_all()

	def get_user(self):
		return codedef.User.query.get(self.user.id)

	def login(self):
		return self.app.post('/auth/', data={'email': 'mail2@vivekn.co.cc', 'password': 'foobar'})

	def logout(self):
		return self.app.post('/logout/')

	def test_problem_eval(self):
		"""
		Creates a problem and a testcase and evaluates submissions against it
		"""
		problem = codedef.Problem(
			title="The answer to life, universe and everything",
			description="Douglas Adams",
			sample_input="",
			sample_output="42",
			weight=2,
			start_time=date_to_str(datetime.now()),
			end_time=date_to_str(datetime.now() + timedelta(hours=3))
			)
		codedef.db.session.add(problem)
		codedef.db.session.commit()
		case = codedef.TestCase(problem=problem, ifile="", ofile="42")
		codedef.db.session.add(case)
		codedef.db.session.commit()
		correct = codedef.Submission(case=case, user=self.user, content="42")
		incorrect = codedef.Submission(case=case, user=self.user, content="26")
		assert correct.evaluate() == True
		assert incorrect.evaluate() == False

	def test_using_http(self):
		self.login()
		assert self.get_user().score == 0
		self.app.post('/problem-editor/new/', data=dict(
			title="The answer to life, universe and everything",
			description="Douglas Adams",
			sample_input="",
			sample_output="42",
			weight="2",
			start_time=date_to_str(datetime.now()),
			end_time=date_to_str(datetime.now() + timedelta(hours=3))
			))
		
		self.app.post('/testcase-editor/new/', data=dict(
			problem_id="1",
			ifile=(StringIO(""), "input.txt"),
			ofile=(StringIO("42"), "output.txt")
			))

		rv = self.app.post('/submit/', data=dict(
			case_id="1",
			source=(StringIO("print 22"), "src.py"),
			output=(StringIO("22"), "output")
			))

		assert self.get_user().score == 0 # Wrong submission

		self.app.post('/submit/', data=dict(
			case_id="1",
			source=(StringIO("print 42"), "src.py"),
			output=(StringIO("42"), "output")
			))


		assert self.get_user().score == 2 # Correct submission



if __name__ == '__main__':
	unittest.main()
