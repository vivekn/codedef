from codedef import db, Problem, TestCase
import datetime

db.drop_all()
db.create_all()

problem_text = """<p>
5 pirates of different ages have a treasure of 100 gold coins. On their ship, they decide to split the coins using this scheme: </p>

<p> The oldest pirate proposes how to share the coins, and all pirates (including the oldest) vote for or against it. 
If 50% or more of the pirates vote for it, then the coins will be shared that way. Otherwise, the pirate proposing the 
scheme will be thrown overboard, and the process is repeated with the pirates that remain. </p>

<p>If a pirate would get same number of coins if he voted for against the proposal, he will vote against it.
Assuming all the pirates are equally intelligent, greedy and do not wish to die, what should be the scheme??</p>
"""

text = """Find the sum of all the prime numbers upto N. You will be given an input file with a single line with a number N. <br>
Warning: N can be quite large (around 100000).
"""
primes = Problem(
    title="Primes Sum",
    description=text,
    sample_input="10",
    sample_output="17",
    weight=4,
    start_time=datetime.datetime.now(),
    end_time=datetime.datetime(2012,3,24)
    )
db.session.add(primes)
db.session.commit()

case1 = TestCase(primes, "90706", "376889162")
case2 = TestCase(primes, "99062", "446235061")
case3 = TestCase(primes, "99358", "448714920")
case4 = TestCase(primes, "91645", "384184524")
case5 = TestCase(primes, "100000", "454396537")

db.session.add(case1)
db.session.add(case2)
db.session.add(case3)
db.session.add(case4)
db.session.add(case5)

db.session.commit()

