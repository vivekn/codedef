from flask import Flask, render_template, session, flash, request, redirect, url_for, Response, abort, g
from flask_utils import login_required
from flaskext.sqlalchemy import SQLAlchemy
from random import choice
from functools import wraps
from os.path import abspath, join, dirname
from werkzeug.security import check_password_hash, generate_password_hash
import datetime
import hashlib

#TODO Custom admin views, decorator

app = Flask(__name__)
app.config.from_pyfile('config.py')
app.debug = True
app.secret_key ='\xf3Oj\xb2\x91\xfdme\x07p\xe4\x86\xdd\x19\x84T\x1d\xbe\xbf:\x10\xcd\x1f\xdf'
db = SQLAlchemy(app)

def admin_required(f):
    @wraps(f)
    def decorated_function(*args, **kwargs):
        if 'user' not in session:
            abort(401)
        elif User.query.get(session['user']).is_admin:
            return f(*args, **kwargs)
        else: abort(401)
    return decorated_function


def get_user():
    return User.query.get(session['user'])
app.jinja_env.globals['get_user'] = get_user

def md5_hash(value):
    return hashlib.md5(unicode(value)).hexdigest()

@app.route('/')
def index():
    if 'user' in session:
        return redirect(url_for('home'))
    return render_template('index.html')

@app.route('/auth/', methods=['POST'])
def authenticate():
    email = request.form['email']
    password = request.form['password']
    user = User.query.filter_by(email=email).first()
    if user is not None and (user.check_password(password)):
        session['user'] = user.id
        g.is_admin = user.is_admin
        return redirect(url_for('home'))
    flash("login failed", 'login')
    return redirect(url_for('index'))

@app.route('/register/', methods=['POST'])
def register():
    email = request.form['email']
    if not User.query.filter_by(email=email).first():
        password = request.form['password']
        name = request.form['name']
        # college = request.form['college']
        # branch = request.form['branch']
        # year = int(request.form['year'])
        user = User(email, password, name)
        print user.password, generate_password_hash(password)
        db.session.add(user)
        db.session.commit()
        session['user'] = user.id
        return redirect(url_for('home'))
    flash("A user with the same email address exists.", 'register')
    return redirect(url_for('index'))

@app.route('/home/')
def home():
    return render_template('home.html')

@app.route('/problems/<int:pid>/')
def problem_view(pid):
    context = {
        'problem': Problem.query.get_or_404(pid)
        }
    if 'user' in session:
        user = get_user()
        case = choice(TestCase.query.filter_by(problem_id=pid).all())
        session['current_testcase'] = case.id
        context.update({
            'solved': Submission.query.join(Problem).filter((Problem.id == pid)
                            & (Submission.user_id == user.id) & (Submission.solved == True)).first() is not None,
        })
        if datetime.datetime.now() > Problem.query.get(pid).end_time:
            context['elapsed'] = True
    return render_template('problem.html', **context)

@app.route('/problems/list/')
def problem_list():
    problems = Problem.query.order_by(id).all()
    return render_template('plist.html', problems=problems)

@app.route('/problems/latest/')
def today():
    latest = Problem.query.order_by(Problem.id.desc()).limit(1).first_or_404()
    return redirect(url_for('problem_view', pid=latest.id))

@app.route('/submit/', methods=['POST'])
@login_required
def submit():
    case_id = session['current_testcase']
    case = TestCase.query.get_or_404(case_id)
    user = get_user()
    source = request.files['source']
    output = request.files['output']
    sub = Submission(case, user, output.read())
    db.session.add(sub)
    ver = Verification(sub, source.read())
    db.session.add(ver)
    db.session.commit()
    # TODO: Fix this file saving code
    # source.save(join(abspath(dirname(__file__)), '/submissions/%d_%s' % (sub.id, source.filename)))
    if not sub.solved:
        if sub.evaluate():
            flash('You have successfully solved the problem', 'submissions')
        else:
            flash('Incorrect answer', 'submissions')
    else: flash("You've already solved this problem.", 'submissions')
    return redirect(url_for('problem_view', pid=case.problem_id))

@app.route('/leaderboard/', defaults = {'page': 1})
@app.route('/leaderboard/<int:page>/')
def leaderboard(page):
    rankings = User.query.order_by(User.score.desc(), User.penalty).paginate(page)
    return render_template('leaderboard.html', rankings=rankings)

@app.route('/history/', defaults = {'page' : 1})
@app.route('/history/<int:page>')
@login_required
def history(page):
    user = get_user()
    history = user.submissions.order_by(Submission.time.desc()).paginate(page)
    result = lambda sub: "Yes" if sub.solved else "No"
    points = lambda sub: sub.problem.weight if sub.solved else 0
    return render_template('history.html', history=history, result=result, points=points)

@app.route('/download/input<pid>.txt')
def input_file(pid):
    curr = session['current_testcase']
    test_case = TestCase.query.get_or_404(curr)
    return Response(str(test_case.ifile), mimetype='application/text')

@app.route('/faq/')
def faq():
    return render_template('faq.html')

@app.route('/logout/')
def logout():
    if 'user' in session:
        session.pop('user')
    return redirect(url_for('index'))

def url_for_other_page(page):
    args = request.view_args.copy()
    args['page'] = page
    return url_for(request.endpoint, **args)
app.jinja_env.globals['url_for_other_page'] = url_for_other_page

"""
Enigma - Puzzles contest
"""

# class EnigmaProblem(db.Model):
#     id = db.Column(db.Integer, primary_key=True)
#     html = db.Column(db.Text)
#     solution = db.Column(db.String(100))
#     weight1 = db.Column(db.Integer)
#     weight2 = db.Column(db.Integer)
#     weight3 = db.Column(db.Integer)
#     weight4 = db.Column(db.Integer)
#     time1 = db.Column(db.DateTime)
#     time2 = db.Column(db.DateTime)
#     time3 = db.Column(db.DateTime)
#     time4 = db.Column(db.DateTime)
#     submissions = db.relationship('EnigmaSubmission', backref='problem', dynamic='lazy')

#     def __init__(self, html, solution, weight_times):
#         self.html = html
#         self.solution = solution
#         i = 0
#         for weight, time in weight_times[:4]:
#             setattr(self, "weight%d" % i, weight)
#             setattr(self, "time%d" % i, time)
#             i += 1

#     def current_score(self):
#         now = datetime.datetime.now()
#         for i in range(4):
#             weight = getattr(self, 'weight%d' % i, 0)
#             time = getattr(self, 'time%d' % i)
#             if time >= now:
#                 return weight
#         return 0



    
# class EnigmaTeam(db.Model):
#     team_name = db.Column(db.String(100), primary_key=True)
#     score = db.Column(db.Integer, default=0)
#     members = db.Column(db.Text)
#     submissions = db.relationship('EnigmaSubmission', backref='team', dynamic='lazy')

#     def __init__(self, team_name, members=""):
#         self.team_name = team_name
#         self.members = members



# class EnigmaSubmission(db.Model):
#     id = db.Column(db.Integer, primary_key=True)
#     team = db.Column(db.String(100), db.ForeignKey('team.team_name'), index=True)
#     problem = db.Column(db.Integer, db.ForeignKey('problem.id'), index=True)
#     answer = db.Column(db.String(100))
#     solved = db.Column(db.Boolean, default=False)

#     def __init__(self, team, problem, answer):
#         self.team = team
#         self.problem = problem
#         self.answer = answer

#     def evaluate(self):
#         """
#         Check if the submission is correct and accordingly update the status
#         """
#         if not self.solved:
#             problem = EnigmaProblem.query.get(self.problem)
#             self.solved = self.answer.strip() == problem.solution.strip()
#             if self.solved:
#                 team = EnigmaTeam.query.get(self.team)

#                 team.score += problem.current_score()
#                 db.session.add(team)

#             db.session.commit()
#         return self.solved    
            

# @app.route('/enigma/')
# def enigma_home():
#     return render_template('enigma/home.html', latest=None)

# @app.route('/enigma/submit/<int:pid>/', methods=['POST'])
# def enigma_submit(pid):
#     team = EnigmaTeam.query.get(request.form['team'])
#     members = request.form['members']
#     if not team:
#         team = EnigmaTeam(team, members)
#         db.session.add(team)
#     if not team.members:
#         team.members = members

#     answer = request.form['answer']
#     problem = EnigmaProblem.query.get(pid)
#     submission = EnigmaSubmission(team, problem.id, answer)
#     db.session.add(submission)
#     submission.evaluate()
#     db.session.commit()
#     flash("Thank you. We have received your submission.", "enigma")
#     return redirect(url_for('enigma_home'))

# @app.route('/enigma/archive/')
# def enigma_archive():
#     problems = EnigmaProblem.query.all()
#     return render_template('enigma/archive/', problems=problems)

# @app.route('/enigma/contacts/')
# def enigma_contacts():
#     return render_template('enigma/contacts.html')

"""
Problem Editor
"""

@app.route('/problem-editor/')
@admin_required
def problem_editor():
    """
    List of problems with edit buttons, create, delete buttons etc
    """
    problems = Problem.query.all()
    return render_template('admin/problem_list.html', problems=problems)

@app.route('/problem-editor/new/', methods=['GET', 'POST'])
@admin_required
def problem_new():
    if request.method == 'GET': 
        return render_template('admin/new_problem.html')
    elif request.method == 'POST':
        params = dict()
        for key in request.form:
            params[key] = request.form[key]
        problem = Problem(**params)
        db.session.add(problem)
        db.session.commit()
        flash('New problem created successfully', 'admin')
        return redirect(url_for('problem_editor'))

@app.route('/problem-editor/update/<int:pid>/', methods=['GET', 'POST'])
@admin_required
def problem_update(pid):
    problem = Problem.query.get_or_404(pid)
    if request.method == 'GET':
        return render_template('admin/problem.html', problem=problem)
    elif request.method == 'POST':
        for key, value in request.form.items():
            if key in ['start_time', 'end_time']:
                setattr(problem, key, datetime.datetime.strptime(value, app.config['TIMEFORMAT']))
            else: setattr(problem, key, value)
        db.session.commit()
        flash('Problem updated successfully', 'admin')
        return redirect(url_for('problem_editor'))

@app.route('/testcase-editor/')
@admin_required
def testcase_editor():
    """
    List of testcases with edit buttons, create, delete buttons etc
    """
    testcases = TestCase.query.all()
    return render_template('admin/testcase_list.html', testcases=testcases)

@app.route('/testcase-editor/new/', methods=['GET', 'POST'])
@admin_required
def testcase_new():
    if request.method == 'GET': 
        return render_template('admin/new_testcase.html')
    elif request.method == 'POST':
        problem_id = int(request.form['problem_id'])
        problem = Problem.query.get(problem_id)
        if not problem: 
            flash('The problem does not exist', 'admin')
            return redirect(url_for('testcase_new'))

        ifile = request.files['ifile'].read()
        ofile = request.files['ofile'].read()
        testcase = TestCase(problem, ifile, ofile)
        db.session.add(testcase)
        db.session.commit()
        flash('New test case created successfully', 'admin')
        return redirect(url_for('testcase_editor'))


"""
Models
"""

class User(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    email = db.Column(db.String(120), unique=True, index=True)
    password = db.Column(db.String(80))
    name = db.Column(db.String(80))
    submissions = db.relationship('Submission', backref='user', lazy='dynamic')
    created = db.Column(db.DateTime)
    score = db.Column(db.Float, default=0)
    penalty = db.Column(db.DateTime, default=datetime.datetime(1, 1, 1))
    is_admin = db.Column(db.Boolean)

    def __init__(self, email, password, name, is_admin=False):
        self.email = email
        self.set_password(password)
        self.name = name
        self.created = datetime.datetime.now()
        self.is_admin = is_admin or email in app.config['ADMINS']

    def set_password(self, password):
        self.password = generate_password_hash(password)

    def check_password(self, password):
        return check_password_hash(self.password, password)

    def __repr__(self):
        return '<User %r>' % self.email

def date_to_template(date_time):
    """
    Converts date to template readable form
    """
    return date_time.strftime(app.config['TIMEFORMAT'])

def string_to_date(string):
    return datetime.datetime.strptime(string, app.config['TIMEFORMAT'])

class Problem(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    title = db.Column(db.String(80))
    description = db.Column(db.Text)
    sample_input = db.Column(db.Text)
    sample_output = db.Column(db.Text)
    testcases = db.relationship('TestCase', backref='problem')
    submissions = db.relationship('Submission', backref='problem')
    weight = db.Column(db.Float)
    start_time = db.Column(db.DateTime)
    end_time = db.Column(db.DateTime)

    def __init__(self, title, description, sample_input, sample_output, weight, start_time, end_time):
        self.title = title
        self.description = description
        self.sample_input = sample_input
        self.sample_output = sample_output
        self.weight = float(weight or 0)
         # The js plugin returns strings
        self.start_time = start_time if isinstance(start_time, datetime.datetime) else string_to_date(start_time)
        self.end_time = end_time if isinstance(end_time, datetime.datetime) else string_to_date(end_time)

    def get_start_time(self):
        return date_to_template(self.start_time)

    def get_end_time(self):
        return date_to_template(self.end_time)

class TestCase(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    problem_id = db.Column(db.Integer, db.ForeignKey('problem.id'), index=True)
    ifile = db.Column(db.Text)
    ofile = db.Column(db.Text)
    code = db.Column(db.String(80), index=True)
    submissions = db.relationship('Submission', backref='test_case', primaryjoin="Submission.case_id==TestCase.id")

    def __init__(self, problem, ifile, ofile):
        self.problem_id = problem.id
        self.ifile = ifile
        self.ofile = ofile
        self.code = md5_hash(ifile)


    def get_problem_name(self):
        return Problem.query.get(self.problem_id).title

class Submission(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    case_id = db.Column(db.Integer, db.ForeignKey('test_case.id'), index=True)
    problem_id = db.Column(db.Integer, db.ForeignKey('problem.id'), index=True)
    user_id = db.Column(db.Integer, db.ForeignKey('user.id'), index=True)
    content = db.Column(db.Text)
    time = db.Column(db.DateTime)
    solved = db.Column(db.Boolean, default=False)
    verification = db.relationship('Verification', backref='submission')


    def __init__(self, case, user, content):
        self.content = content
        self.user_id = user.id
        self.case_id = case.id
        self.problem_id = TestCase.query.get(case.id).problem_id
        self.time = datetime.datetime.now()

    def evaluate(self):
        """
        Check if the submission is correct and accordingly update the status
        """
        if not self.solved:
            case = TestCase.query.get(self.case_id).ofile.splitlines()
            self.solved = all(x == y for x, y in zip(self.content.splitlines(), case))
            self.solved = False if len(case) != len(self.content.splitlines()) else self.solved
            if self.solved:
                user = User.query.get(self.user_id)
                problem = Problem.query.get(self.problem_id)
                user.score += problem.weight
                user.penalty += (self.time - problem.start_time)
                db.session.add(user)

            db.session.commit()
        return self.solved

class Verification(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    sub_id = db.Column(db.Integer, db.ForeignKey('submission.id'), index=True)
    content = db.Column(db.Text)

    def __init__(self, submission, content):
        self.sub_id = submission.id
        self.content = content


if __name__ == '__main__':
    app.run()
